@echo off

setlocal EnableDelayedExpansion

set audio_bitrate=128k

set exe=library\ffmpeg.exe
set input_dir=ffinput
set output_dir=ffoutput

if not exist "%output_dir%" (
	mkdir "%output_dir%"
)

cd "%input_dir%"

for %%i in ("*.3gp" "*.avi" "*.flv" "*.m4a" "*.m4v" "*.mkv" "*.mp3" "*.mp4" "*.mpeg" "*.mpg" "*.mov" "*.ogg" "*.ogv" "*.rm" "*.rmvb" "*.vob" "*.wav" "*.webm" "*.wma" "*.wmv") do (
	"..\%exe%" -y -i "%%i" -vn -acodec libmp3lame -ab "%audio_bitrate%" -ar 44100 -ac 2 -map_metadata -1 "..\%output_dir%\%%i.mp3"
)